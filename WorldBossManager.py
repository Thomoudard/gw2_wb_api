from json import load
from WorldBoss import WorldBoss

from typing import List
from datetime import datetime, timedelta

class WorldBossManager:
    world_boss_list: list[WorldBoss] = []

    def __init__(self, json_data_file: str) -> None:
        with open(json_data_file, 'r') as json_file:
            world_boss_data = load(json_file)

        for boss_info in world_boss_data:
            self.world_boss_list.append(WorldBoss(**boss_info))
    
    def __str__(self) -> str:
        return '\n'.join(str(boss) for boss in self.world_boss_list)
    
    def find_by_id_name(self, id_name: str) -> WorldBoss:
        for boss in self.world_boss_list:
            if boss.id_name == id_name:
                return boss
        return None    

test = WorldBossManager("world_boss.json")
print(test.find_by_id_name('tequatl_the_sunless'))