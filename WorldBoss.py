from typing import Any
from datetime import datetime, timezone

class WorldBoss():
    id_name: str
    name: str
    zone: str
    tp_id: str
    dt: list[datetime] = []

    def __init__(self, id_name: str, name: str, zone: str, tp_id: str, lst_time: list[str]) -> None:
        self.id_name = id_name
        self.name = name
        self.zone = zone
        self.tp_id = tp_id
        self.dt = [
            datetime(
                year=1970,
                month=1,
                day=1,
                hour=int(dt.split(':')[0]),
                minute=int(dt.split(':')[1]),
                tzinfo=timezone.utc
            ) 
            for dt in lst_time
        ]

    def __getattribute__(self, __name: str) -> Any:
        return object.__getattribute__(self, __name)

    def __str__(self) -> str:
        spawn_times = [dt.astimezone(datetime.now().astimezone().tzinfo).strftime('%H:%M') for dt in self.dt]
        spawn_times_str = ', '.join(spawn_times)

        return f"The world boss {self.name} in {self.zone} spawns at {spawn_times_str}. Copy-paste this ID in your chatbox {self.tp_id}"
    